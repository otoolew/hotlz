// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HotLZGameModeBase.generated.h"

UENUM()
enum class EGamePlayState 
{
	Running,
	Paused,
	Unknown
};

/**
 * 
 */
UCLASS()
class HOTLZ_API AHotLZGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	AHotLZGameModeBase(const class FObjectInitializer& ObjectInitializer);
};
