// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterPawnMovementComponent.h"
#include "CharacterPawn.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

UCharacterPawnMovementComponent::UCharacterPawnMovementComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	CasualWalkSpeedMultiplier = 0.4f;
	AimDownSightSpeedMultiplier = 0.5f;
	SprintSpeedMultiplier = 1.5f;
}

//============================================================================================
//Replication
//============================================================================================
void UCharacterPawnMovementComponent::UpdateFromCompressedFlags(uint8 Flags)
{
	Super::UpdateFromCompressedFlags(Flags);
	//The Flags parameter contains the compressed input flags that are stored in the saved move.
	//UpdateFromCompressed flags simply copies the flags from the saved move into the movement component.
	//It basically just resets the movement component to the state when the move was made so it can simulate from there.
	bWantsToCasuallyWalk = (Flags & FSavedMove_Character::FLAG_Custom_0) != 0;
	bWantsToAimDownSight = (Flags & FSavedMove_Character::FLAG_Custom_1) != 0;
	bWantsToSprint = (Flags & FSavedMove_Character::FLAG_Custom_2) != 0;

//TODO: Add other Wants
}

uint8 UCharacterPawnMovementComponent::FSavedMove_CharacterPawnMovement::GetCompressedFlags() const
{
	uint8 Result = Super::GetCompressedFlags();


	return Result;
}

class FNetworkPredictionData_Client* UCharacterPawnMovementComponent::GetPredictionData_Client() const
{
	check(PawnOwner != NULL);
	check(PawnOwner->Role < ROLE_Authority);

	if (!ClientPredictionData)
	{
		UCharacterPawnMovementComponent* MutableThis = const_cast<UCharacterPawnMovementComponent*>(this);

		MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_CharacterPawnMovement(*this);
		MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = 92.f;
		MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = 140.f;
	}

	return ClientPredictionData;
}

void UCharacterPawnMovementComponent::FSavedMove_CharacterPawnMovement::Clear()
{
	Super::Clear();
	SavedMoveDirection = FVector::ZeroVector;
	bSavedWantsToCasuallyWalk = false;
	bSavedWantsToAimDownSight = false;
	bSavedWantsToSprint = false;

}

void UCharacterPawnMovementComponent::OnMovementUpdated(float DeltaTime, const FVector& OldLocation, const FVector& OldVelocity)
{
	Super::OnMovementUpdated(DeltaTime, OldLocation, OldVelocity);

	if (!CharacterOwner)
	{
		return;
	}

	//Store movement vector
	if (PawnOwner->IsLocallyControlled())
	{
		MoveDirection = PawnOwner->GetLastMovementInputVector();
	}
	//Send movement vector to server
	//if (PawnOwner->Role < ROLE_Authority)
	//{
	//	ServerSetMoveDirection(MoveDirection);
	//}

}

void UCharacterPawnMovementComponent::FSavedMove_CharacterPawnMovement::SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, FNetworkPredictionData_Client_Character& ClientData)
{
	Super::SetMoveFor(Character, InDeltaTime, NewAccel, ClientData);

	UCharacterPawnMovementComponent* CharacterMovement = Cast<UCharacterPawnMovementComponent>(Character->GetCharacterMovement());
	if (CharacterMovement)
	{
		bSavedWantsToCasuallyWalk = CharacterMovement->bWantsToCasuallyWalk;
		bSavedWantsToSprint = CharacterMovement->bWantsToSprint;
		bSavedWantsToAimDownSight = CharacterMovement->bWantsToCasuallyWalk;

	}
}

void UCharacterPawnMovementComponent::FSavedMove_CharacterPawnMovement::PrepMoveFor(ACharacter* Character)
{
	Super::PrepMoveFor(Character);

	UCharacterPawnMovementComponent* CharacterMovement = Cast<UCharacterPawnMovementComponent>(Character->GetCharacterMovement());
	if (CharacterMovement)
	{
		//This is just the exact opposite of SetMoveFor. It copies the state from the saved move to the movement
		//component before a correction is made to a client.
		CharacterMovement->MoveDirection = SavedMoveDirection;
		//Don't update flags here. They're automatically setup before corrections using the compressed flag methods.
	}
}

bool UCharacterPawnMovementComponent::FSavedMove_CharacterPawnMovement::CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const
{
	//Set which moves can be combined together. This will depend on the bit flags that are used.	
	//This pretty much just tells the engine if it can optimize by combining saved moves. There doesn't appear to be
	//any problem with leaving it out, but it seems that it's good practice to implement this anyways.
	if (bSavedWantsToCasuallyWalk != ((FSavedMove_CharacterPawnMovement*)&NewMove)->bSavedWantsToCasuallyWalk)
	{
		return false;
	}

	if (bSavedWantsToAimDownSight != ((FSavedMove_CharacterPawnMovement*)&NewMove)->bSavedWantsToAimDownSight)
	{
		return false;
	}


	if (bSavedWantsToSprint != ((FSavedMove_CharacterPawnMovement*)&NewMove)->bSavedWantsToSprint)
	{
		return false;
	}

	return Super::CanCombineWith(NewMove, Character, MaxDelta);
}


float UCharacterPawnMovementComponent::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const ACharacterPawn* CharacterOwner = Cast<ACharacterPawn>(PawnOwner);
	if (CharacterOwner)
	{
		if (bWantsToCasuallyWalk)
		{
			MaxSpeed *= CasualWalkSpeedMultiplier;
		}
		if (bWantsToSprint && IsMovingForward())
		{
			MaxSpeed *= SprintSpeedMultiplier;
		}
		if (bWantsToAimDownSight)
		{
			MaxSpeed *= AimDownSightSpeedMultiplier;
		}	
	}

	return MaxSpeed;
}

float UCharacterPawnMovementComponent::GetMaxAcceleration() const
{
	float MaxAccel = Super::GetMaxAcceleration();

	if (bWantsToCasuallyWalk)
	{
		MaxAccel *= CasualWalkAccelerationMultiplier;
	}

	if (bWantsToSprint && IsMovingForward())
	{
		MaxAccel *= SprintAccelerationMultiplier;
	}

	if (bWantsToAimDownSight)
	{
		MaxAccel *= AimDownSightAccelerationMultiplier;
	}

	return MaxAccel;
}

float UCharacterPawnMovementComponent::GetMoveVelocity() const
{
	return Super::GetLastUpdateVelocity().Size();
}

float UCharacterPawnMovementComponent::GetDirection() const
{
	FRotator controlRotation = Super::GetLastUpdateRotation();
	FRotator pawnRotation = Super::GetLastUpdateVelocity().Rotation();

	FRotator delta = UKismetMathLibrary::NormalizedDeltaRotator(pawnRotation, controlRotation);

	return delta.Yaw;
}

bool UCharacterPawnMovementComponent::IsMovingForward() const
{
	if (!PawnOwner)
	{
		return false;
	}

	FVector Forward = PawnOwner->GetActorForwardVector();
	FVector MoveDirection = Velocity.GetSafeNormal();

	//Ignore vertical movement
	Forward.Z = 0.0f;
	MoveDirection.Z = 0.0f;

	float VelocityDot = FVector::DotProduct(Forward, MoveDirection);
	return VelocityDot > 0.7f;//Check to make sure difference between headings is not too great.
}



void UCharacterPawnMovementComponent::SetSprinting(bool bSprinting)
{
	bWantsToSprint = bSprinting;
}

void UCharacterPawnMovementComponent::SetCasualWalk(bool bCasuallyWalking)
{
	bWantsToCasuallyWalk = bCasuallyWalking;
}

void UCharacterPawnMovementComponent::SetAimDownSight(bool bAimingDownSight)
{
	bWantsToAimDownSight = bAimingDownSight;
}

bool UCharacterPawnMovementComponent::HandlePendingLaunch()
{
	if (!PendingLaunchVelocity.IsZero() && HasValidData())
	{
		Velocity = PendingLaunchVelocity;
		//Remmed out so our dodge move won't play the falling animation.
		//SetMovementMode(MOVE_Falling);
		PendingLaunchVelocity = FVector::ZeroVector;
		bForceNextFloorCheck = true;
		return true;
	}

	return false;
}

FSavedMovePtr UCharacterPawnMovementComponent::FNetworkPredictionData_Client_CharacterPawnMovement::AllocateNewMove()
{
	return FSavedMovePtr(new FSavedMove_CharacterPawnMovement());
}

UCharacterPawnMovementComponent::FNetworkPredictionData_Client_CharacterPawnMovement::FNetworkPredictionData_Client_CharacterPawnMovement(const UCharacterMovementComponent& ClientMovement) :Super(ClientMovement)
{

}

