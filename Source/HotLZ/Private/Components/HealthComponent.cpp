// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "GameFramework/Actor.h"
#include "CharacterPawn.h"
#include "Engine/GameEngine.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent(const class FObjectInitializer& ObjectInitializer) :Super(ObjectInitializer)
{
	MaxHealth = 100;
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();	

	ACharacterPawn* MyOwner = Cast<ACharacterPawn>(GetOwner());

	if (MyOwner) 
	{
		MyOwner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::HandleTakeAnyDamage);
	}
	Health = MaxHealth;
}

void UHealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstegatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.0f) {
		return;
	}

	Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);
	//if (GEngine)
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Hit Health Comp!"));
	//}
	//UE_LOG(LogTemp, Log, TEXT("Health Changed: %s"), *FString::SanitizeFloat(Health));

	OnHealthChanged.Broadcast(this, Health, Damage, DamageType, InstegatedBy, DamageCauser);
}

bool UHealthComponent::IsAlive() const
{
	return Health > 0;
}

bool UHealthComponent::IsDying() const
{
	return false;
}

bool UHealthComponent::IsDead() const
{
	return false;
}

float UHealthComponent::GetHealth() const
{
	return Health;
}

