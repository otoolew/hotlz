// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterPerceptionComponent.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISenseConfig_Sight.h"

UCharacterPerceptionComponent::UCharacterPerceptionComponent(const class FObjectInitializer& ObjectInitializer) : UAIPerceptionComponent(ObjectInitializer)
{
	TArray<UAISenseConfig*> SensesConfig;
	SightSense = CreateDefaultSubobject<UAISenseConfig_Sight>("UAISense_Sight");

	SensesConfig.Add(SightSense);
}