// Fill out your copyright notice in the Description page of Project Settings.


#include "TeamComponent.h"

// Sets default values for this component's properties
UTeamComponent::UTeamComponent(const class FObjectInitializer& ObjectInitializer)
{

}


bool UTeamComponent::IsHostile(ETeamFaction Value) const
{
	if ((uint8)TeamFaction == (uint8)Value) 
	{
		return false;
	}

	return true;
}


