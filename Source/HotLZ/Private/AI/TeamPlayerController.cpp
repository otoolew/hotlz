// Fill out your copyright notice in the Description page of Project Settings.


#include "TeamPlayerController.h"
#include "PlayerPawn.h"
#include "GameFramework/Pawn.h"
#include "Engine/GameEngine.h"
#include "Runtime/Engine/Public/EngineGlobals.h"


ATeamPlayerController::ATeamPlayerController(const class FObjectInitializer& ObjectInitializer) : APlayerController(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;

	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;

	TeamId = FGenericTeamId(5);
}



void ATeamPlayerController::Tick(float DeltaTime)
{
}

void ATeamPlayerController::SetupInputComponent()
{
	APlayerController::SetupInputComponent();

	InputComponent->BindAction("PauseMenu", IE_Pressed, this, &ATeamPlayerController::OnTogglePauseMenu);

}

void ATeamPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (InPawn) 
	{
		PlayerPawn = Cast<APlayerPawn>(InPawn);
	}
}

void ATeamPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
}

APlayerPawn* ATeamPlayerController::GetPlayerPawn() const
{
	if (PlayerPawn)
	{
		return PlayerPawn;
	}

	return NULL;
}

void ATeamPlayerController::SetGenericTeamId(const FGenericTeamId& Value)
{
	TeamId = Value;
}

FGenericTeamId ATeamPlayerController::GetGenericTeamId() const
{
	return TeamId;
}

ETeamAttitude::Type ATeamPlayerController::GetTeamAttitudeTowards(const AActor& Other) const
{
	if (const APawn* OtherPawn = Cast<APawn>(&Other)) {

		if (const IGenericTeamAgentInterface* TeamAgent = Cast<IGenericTeamAgentInterface>(OtherPawn->GetController()))
		{
			//if (GEngine) 
			//{
			//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Team Agent Cast Success"));
			//}

			//FGenericTeamId OtherTeamID = TeamAgent->GetGenericTeamId();
			//if (TeamAgent->GetGenericTeamId() == GetGenericTeamId())
			//{
			//	if (GEngine) 
			//	{
			//		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Found Ally"));
			//	}
			//	return ETeamAttitude::Neutral;
			//}
			//if (GEngine)
			//{
			//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Found Enemy"));
			//}
			//return ETeamAttitude::Hostile;
			FGenericTeamId OtherTeamID = TeamAgent->GetGenericTeamId();
			if (OtherTeamID == 10) {
				return ETeamAttitude::Friendly;
			}
			else {
				return ETeamAttitude::Hostile;
			}
		}
	}

	return ETeamAttitude::Neutral;
}
APlayerPawnHUD* ATeamPlayerController::GetPlayerPawnHUD() const
{
	return nullptr;
}
bool ATeamPlayerController::IsPauseMenuVisible() const
{
	return false;
}
void ATeamPlayerController::OnTogglePauseMenu()
{
	//if (GEngine->GameViewport == nullptr)
	//{
	//	return;
	//}

	//// this is not ideal, but necessary to prevent both players from pausing at the same time on the same frame
	//UWorld* GameWorld = GEngine->GameViewport->GetWorld();

	//for (auto It = GameWorld->GetControllerIterator(); It; ++It)
	//{
	//	AShooterPlayerController* Controller = Cast<AShooterPlayerController>(*It);
	//	if (Controller && Controller->IsPaused())
	//	{
	//		return;
	//	}
	//}

	//// if no one's paused, pause
	//if (ShooterIngameMenu.IsValid())
	//{
	//	ShooterIngameMenu->ToggleGameMenu();
	//}
}
bool ATeamPlayerController::IsGameInputAllowed() const
{
	return false;
}
bool ATeamPlayerController::IsGameMenuVisible() const
{
	return false;
}
bool ATeamPlayerController::SetPause(bool bPause, FCanUnpause CanUnpauseDelegate)
{
	return false;
}
//void ATeamPlayerController::OnToggleInGameMenu()
//{
//	if (GEngine->GameViewport == nullptr)
//	{
//		return;
//	}
//
//	// this is not ideal, but necessary to prevent both players from pausing at the same time on the same frame
//	//UWorld* GameWorld = GEngine->GameViewport->GetWorld();
//	//GetWorld();
//	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
//	{
//		ATeamPlayerController* Controller = Cast<ATeamPlayerController>(*It);
//		if (Controller && Controller->IsPaused())
//		{
//			return;
//		}
//	}
//
//	// if no one's paused, pause
//	//if (ShooterIngameMenu.IsValid())
//	//{
//	//	ShooterIngameMenu->ToggleGameMenu();
//	//}
//}
