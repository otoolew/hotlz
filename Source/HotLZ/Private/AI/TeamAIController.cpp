// Fill out your copyright notice in the Description page of Project Settings.


#include "TeamAIController.h"
#include "Engine/GameEngine.h"
#include "BotPawn.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Bool.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISense_Touch.h"
//#include "CharacterPerceptionComponent.h"
//#include "Perception/AISense_Sight.h"
//#include "Perception/AISense_Damage.h"

ATeamAIController::ATeamAIController(const class FObjectInitializer& ObjectInitializer) : AAIController(ObjectInitializer)
{
	BlackboardComp = ObjectInitializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackBoardComp"));

	BrainComponent = BehaviorComp = ObjectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComp"));

	//CharacterPerceptionComp = ObjectInitializer.CreateDefaultSubobject<UCharacterPerceptionComponent>(this, TEXT("CharacterPerceptionComp"));

	TeamId = FGenericTeamId(10);

}

void ATeamAIController::SetGenericTeamId(const FGenericTeamId& Value)
{
	TeamId = Value;
}

FGenericTeamId ATeamAIController::GetGenericTeamId() const
{
	return TeamId;
}

ETeamAttitude::Type ATeamAIController::GetTeamAttitudeTowards(const AActor& Other) const
{
	if (const APawn* OtherPawn = Cast<APawn>(&Other)) {

		if (const IGenericTeamAgentInterface* TeamAgent = Cast<IGenericTeamAgentInterface>(OtherPawn->GetController()))
		{
			//if (GEngine) 
			//{
			//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Team Agent Cast Success"));
			//}

			////FGenericTeamId OtherTeamID = TeamAgent->GetGenericTeamId();
			//if (TeamAgent->GetGenericTeamId() == GetGenericTeamId()) 
			//{
			//	return ETeamAttitude::Friendly;
			//}

			//return ETeamAttitude::Hostile;
			//Create an alliance with Team with ID 10 and set all the other teams as Hostiles:
			FGenericTeamId OtherTeamID = TeamAgent->GetGenericTeamId();
			if (OtherTeamID == 5) {
				return ETeamAttitude::Friendly;
			}
			else {
				return ETeamAttitude::Hostile;
			}
		}
	}

	return ETeamAttitude::Neutral;
}
void ATeamAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	ABotPawn* Bot = Cast<ABotPawn>(InPawn);

	// start behavior
	if (Bot && Bot->BotBehavior)
	{
		if (Bot->BotBehavior->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*Bot->BotBehavior->BlackboardAsset);
		}

		EnemyKeyID = BlackboardComp->GetKeyID("EnemyTarget");

		BehaviorComp->StartTree(*(Bot->BotBehavior));


		Bot->OnActorHit.AddDynamic(this, &ATeamAIController::HandlePawnHit);		
	}


}


void ATeamAIController::OnUnPossess()
{
	Super::OnUnPossess();

	BehaviorComp->StopTree();
}

void ATeamAIController::SetEnemy(ACharacterPawn* InCharacterPawn)
{
	if (BlackboardComp)
	{
		if (GetEnemy())
		{
			if (GetEnemy() != InCharacterPawn)
			{
				BlackboardComp->SetValue<UBlackboardKeyType_Object>(EnemyKeyID, InCharacterPawn);
			}
		}
		else
		{
			BlackboardComp->SetValue<UBlackboardKeyType_Object>(EnemyKeyID, InCharacterPawn);
		}


		SetFocus(InCharacterPawn);
	}
}

ACharacterPawn* ATeamAIController::GetEnemy() const
{
	if (BlackboardComp)
	{
		return Cast<ACharacterPawn>(BlackboardComp->GetValue<UBlackboardKeyType_Object>(EnemyKeyID));
	}

	return NULL;
}

void ATeamAIController::SetLineOfSightKey(bool Value)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValue<UBlackboardKeyType_Bool>(HasLineOfSightKeyID, Value);
	}
}


void ATeamAIController::AttackEnemy()
{

}

void ATeamAIController::HandlePawnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	//if (GEngine) 
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("AIController: Controlled Pawn Hit!"));
	//}
	UAIPerceptionSystem* PerceptionSystem = UAIPerceptionSystem::GetCurrent(this);
	if (PerceptionSystem)
	{
		FAITouchEvent Event(this, OtherActor, Hit.Location);
		PerceptionSystem->OnEvent(Event);
	}

	//UAIPerceptionComponent  PerceptionComp = GetComponentByClass<UAIPerceptionComponent>();
}

//void ATeamAIController::UpdateControlRotation(float DeltaTime, bool bUpdatePawn)
//{
//	// Look toward focus
//	FVector FocalPoint = GetFocalPoint();
//	if (!FocalPoint.IsZero() && GetPawn())
//	{
//		FVector Direction = FocalPoint - GetPawn()->GetActorLocation();
//		FRotator NewControlRotation = Direction.Rotation();
//
//		NewControlRotation.Yaw = FRotator::ClampAxis(NewControlRotation.Yaw);
//
//		SetControlRotation(NewControlRotation);
//
//		APawn* const P = GetPawn();
//		if (P && bUpdatePawn)
//		{
//			P->FaceRotation(NewControlRotation, DeltaTime);
//		}
//
//	}
//}
