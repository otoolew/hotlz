// Fill out your copyright notice in the Description page of Project Settings.


#include "Door.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ADoor::ADoor(const class FObjectInitializer& ObjectInitializer) : AActor(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	//DoorFrameMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorFrameMesh"));
	//DoorFrameMesh->bCastDynamicShadow = true;
	//DoorFrameMesh->CastShadow = true;
	//DoorFrameMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);


	//DoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorMesh"));
	//DoorMesh->bCastDynamicShadow = true;
	//DoorMesh->CastShadow = true;
	//DoorMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	AActor::BeginPlay();
	
}

// Called every frame
void ADoor::Tick(float DeltaTime)
{
	AActor::Tick(DeltaTime);

}

