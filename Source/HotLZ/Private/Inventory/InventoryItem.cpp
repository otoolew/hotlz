// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryItem.h"

#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

// Sets default values
AInventoryItem::AInventoryItem(const class FObjectInitializer& ObjectInitializer) : AActor(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

void AInventoryItem::OnInteraction_Implementation()
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Someone is Interacting with me!!"));
	}
}

// Called when the game starts or when spawned
void AInventoryItem::BeginPlay()
{
	AActor::BeginPlay();
	
}

// Called every frame
void AInventoryItem::Tick(float DeltaTime)
{
	AActor::Tick(DeltaTime);

}

