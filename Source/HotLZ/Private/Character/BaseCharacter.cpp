// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"
#include "HotLZ.h"
#include "Engine/GameEngine.h"
#include "Kismet/KismetMathLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PlayerController.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "CharacterPawnMovementComponent.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "HealthComponent.h"
#include "Weapon/BaseWeapon.h"

// Sets default values
ABaseCharacter::ABaseCharacter(const class FObjectInitializer& ObjectInitializer) : ACharacter(ObjectInitializer.SetDefaultSubobjectClass<UCharacterPawnMovementComponent>(ACharacter::CharacterMovementComponentName))
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Character Movement Components
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;

	// Capsule and Mesh Collison
	GetCapsuleComponent()->InitCapsuleSize(55.f, 90.0f);
	GetMesh()->SetCollisionProfileName(FName("BlockAll"));

	// Health Comp
	HealthComp = CreateDefaultSubobject<UHealthComponent>("HealthComp");

	// AI Perception
	AIPerceptionStimuliSourceComp = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>("AIPerceptionStimuliSource");

	// CameraBoom
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->bAbsoluteRotation = true;
	CameraBoom->TargetArmLength = 1200.0f;
	CameraBoom->bUsePawnControlRotation = false;
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Camera
	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComp->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComp->bUsePawnControlRotation = false;

	//// Create a decal in the world to show the cursor's location
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	//CursorToWorld->SetupAttachment(RootComponent);
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());


	// Init property values
	bIsAImingDownSight = false;
	AimDownSightSpeedModifier = 0.5f;
	SprintingSpeedModifier = 1.5f;
	WeaponAttachSocketName = "WeaponSocket";
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	HealthComp->OnHealthChanged.AddDynamic(this, &ABaseCharacter::OnHealthChanged);

	FActorSpawnParameters WeaponSpawnParams;
	WeaponSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	EquippedWeapon = GetWorld()->SpawnActor<ABaseWeapon>(StartingWeapon, FVector::ZeroVector, FRotator::ZeroRotator, WeaponSpawnParams);
	if (EquippedWeapon)
	{
		EquippedWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponAttachSocketName);
		EquippedWeapon->SetOwner(this);
	}
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ABaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABaseCharacter::MoveRight);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ABaseCharacter::StartCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ABaseCharacter::EndCrouch);

	PlayerInputComponent->BindAction("AimDownSight", IE_Pressed, this, &ABaseCharacter::StartAimAtTarget);
	PlayerInputComponent->BindAction("AimDownSight", IE_Released, this, &ABaseCharacter::StopAimAtTarget);

	PlayerInputComponent->BindAction("FireWeapon", IE_Pressed, this, &ABaseCharacter::StartFireWeapon);
}

FVector ABaseCharacter::GetPawnViewLocation() const
{
	return FVector();
}

//void ABaseCharacter::PlayerCursorInput()
//{
//	//if (CursorToWorld != nullptr)
//	//{
//	APlayerController* PlayerController = Cast<APlayerController>(GetController());
//	if (PlayerController != nullptr)
//	{
//		FHitResult TraceHitResult;
//		PlayerController->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
//		FVector CursorFV = TraceHitResult.ImpactNormal;
//		FRotator CursorR = CursorFV.Rotation();
//		//CursorToWorld->SetWorldLocation(TraceHitResult.Location);
//		//CursorToWorld->SetWorldRotation(CursorR);
//	}
//	//}
//}

float ABaseCharacter::GetSpeed() const
{
	return GetVelocity().Size();
}

float ABaseCharacter::GetDirection() const
{
	FRotator controlRotation = GetControlRotation();
	FRotator pawnRotation = GetVelocity().Rotation();

	FRotator delta = UKismetMathLibrary::NormalizedDeltaRotator(pawnRotation, controlRotation);

	return delta.Yaw;
}

void ABaseCharacter::MoveForward(float value)
{
	if (value != 0.0f)
	{
		AddMovementInput(FVector(1, 0, 0), value);
	}
	//if ((Controller != NULL) && (Value != 0.0f))
	//{
	//	// find out which way is forward
	//	const FRotator Rotation = Controller->GetControlRotation();
	//	const FRotator YawRotation(0, Rotation.Yaw, 0);

	//	// get forward vector
	//	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	//	AddMovementInput(Direction, Value);
	//}
}

void ABaseCharacter::MoveRight(float value)
{
	if (value != 0.0f)
	{
		AddMovementInput(FVector(0, 1, 0), value);
	}
}

void ABaseCharacter::StartCrouch()
{
	Crouch();
}

void ABaseCharacter::EndCrouch()
{
	UnCrouch();
}

float ABaseCharacter::GetAimDownSightSpeedModifier() const
{
	return AimDownSightSpeedModifier;
}

float ABaseCharacter::GetSprintingSpeedModifier() const
{
	return SprintingSpeedModifier;
}

void ABaseCharacter::OnHealthChanged(UHealthComponent* HealthComponent, float CurrentHealth, float HealthChangeValue, const UDamageType* DamageType, AController* InstegatedBy, AActor* DamageCauser)
{
	if (CurrentHealth <= 0.0f && !bIsDead)
	{
		bIsDead = true;

		DetachFromControllerPendingDestroy();

		UCapsuleComponent* CapsuleComp = GetCapsuleComponent();
		CapsuleComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		CapsuleComp->SetCollisionResponseToAllChannels(ECR_Ignore);

		USkeletalMeshComponent* Mesh3P = GetMesh();
		if (Mesh3P)
		{
			Mesh3P->SetCollisionProfileName(TEXT("Ragdoll"));
		}
		SetActorEnableCollision(true);
		//GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		SetRagdollPhysics();
		///* Apply physics impulse on the bone of the enemy skeleton mesh we hit (ray-trace damage only) */
		//if (DamageEvent.IsOfType(FPointDamageEvent::ClassID))
		//{
		//	FPointDamageEvent PointDmg = *((FPointDamageEvent*)(&DamageEvent));
		//	{
		//		// TODO: Use DamageTypeClass->DamageImpulse
		//		Mesh3P->AddImpulseAtLocation(PointDmg.ShotDirection * 12000, PointDmg.HitInfo.ImpactPoint, PointDmg.HitInfo.BoneName);
		//	}
		//}
		//if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
		//{
		//	FRadialDamageEvent RadialDmg = *((FRadialDamageEvent const*)(&DamageEvent));
		//	{
		//		Mesh3P->AddRadialImpulse(RadialDmg.Origin, RadialDmg.Params.GetMaxRadius(), 100000 /*RadialDmg.DamageTypeClass->DamageImpulse*/, ERadialImpulseFalloff::RIF_Linear);
		//	}
		//}
	}
}

void ABaseCharacter::SetRagdollPhysics()
{
	bool bInRagdoll = false;
	USkeletalMeshComponent* Mesh3P = GetMesh();

	if (IsPendingKill())
	{
		bInRagdoll = false;
	}
	else if (!Mesh3P || !Mesh3P->GetPhysicsAsset())
	{
		bInRagdoll = false;
	}
	else
	{
		Mesh3P->SetAllBodiesSimulatePhysics(true);
		Mesh3P->SetSimulatePhysics(true);
		Mesh3P->WakeAllRigidBodies();
		Mesh3P->bBlendPhysics = true;

		bInRagdoll = true;
	}

	UCharacterMovementComponent* CharacterComp = Cast<UCharacterMovementComponent>(GetMovementComponent());
	if (CharacterComp)
	{
		CharacterComp->StopMovementImmediately();
		CharacterComp->DisableMovement();
		CharacterComp->SetComponentTickEnabled(false);
	}

	if (!bInRagdoll)
	{
		// Immediately hide the pawn
		TurnOff();
		SetActorHiddenInGame(true);
		SetLifeSpan(1.0f);
	}
	else
	{
		SetLifeSpan(3.0f);
	}
}

void ABaseCharacter::StartFireWeapon()
{
	if (EquippedWeapon)
	{
		EquippedWeapon->FireWeapon();
	}
}

void ABaseCharacter::StopFireWeapon()
{
}

bool ABaseCharacter::IsAimingDownSight() const
{
	return bIsAImingDownSight;
}

void ABaseCharacter::StartAimAtTarget()
{
	bIsAImingDownSight = true;
}

void ABaseCharacter::StopAimAtTarget()
{
	bIsAImingDownSight = false;
}

void ABaseCharacter::FocusAimAtTarget()
{
	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	if (PlayerController != nullptr)
	{
		FHitResult TraceHitResult;

		PlayerController->GetHitResultUnderCursor(COLLISION_WEAPON, true, TraceHitResult);

		FVector EyeLocation;
		FRotator EyeRotation;
		GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FRotator lookRotation = UKismetMathLibrary::FindLookAtRotation(EyeLocation, TraceHitResult.Location);

		FRotator rot = FMath::RInterpTo(GetActorRotation(), lookRotation, GetWorld()->GetDeltaSeconds(), 360.0f);

		PlayerController->SetControlRotation(rot);
	}
}
