// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterPawn.h"
#include "HotLZ.h"
#include "Engine/GameEngine.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "CharacterPawnMovementComponent.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "HealthComponent.h"
#include "Weapon/BaseWeapon.h"
#include "Weapon/RangedWeapon.h"
#include "Perception/AISense_Damage.h"
#include "Perception/AISense_Touch.h"
#include "Perception/AISense_Hearing.h"

#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>
// Sets default values
ACharacterPawn::ACharacterPawn(const class FObjectInitializer& ObjectInitializer) : ACharacter(ObjectInitializer.SetDefaultSubobjectClass<UCharacterPawnMovementComponent>(ACharacter::CharacterMovementComponentName))
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AIPerceptionStimuliSourceComp = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>("AIPerceptionStimuliSource");

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;
	GetCapsuleComponent()->InitCapsuleSize(55.f, 90.0f);

	GetMesh()->SetCollisionProfileName(FName("BlockAll"));

	WeaponAttachSocketName = "WeaponSocket";

}

// Called when the game starts or when spawned
void ACharacterPawn::BeginPlay()
{
	Super::BeginPlay();
	Health = MaxHealth;
	FActorSpawnParameters WeaponSpawnParams;
	WeaponSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	EquippedWeapon = GetWorld()->SpawnActor<ABaseWeapon>(StartingWeapon, FVector::ZeroVector, FRotator::ZeroRotator, WeaponSpawnParams);
	if (EquippedWeapon)
	{
		EquippedWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponAttachSocketName);
		EquippedWeapon->SetOwner(this);
	}
}

// Called every frame
void ACharacterPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACharacterPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

FVector ACharacterPawn::GetPawnViewLocation() const
{
	return Super::GetPawnViewLocation();
}

void ACharacterPawn::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	CharacterPawnMovementComp = Cast<UCharacterPawnMovementComponent>(Super::GetMovementComponent());
}

bool ACharacterPawn::IsInCombat() const
{
	return bIsInCombat;
}

//void ACharacterPawn::TouchSensed(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
//{
//	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
//	{
//		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("I Hit: %s"), *OtherActor->GetName()));
//	}
//}


void ACharacterPawn::StartCrouch()
{
	Crouch();
}

void ACharacterPawn::EndCrouch()
{
	UnCrouch();
}

void ACharacterPawn::StartCasualWalk()
{
	bIsCasuallyWalking = true;
}

void ACharacterPawn::EndCasualWalk()
{
	bIsCasuallyWalking = false;
}

bool ACharacterPawn::IsCasuallyWalking() const
{
	return bIsCasuallyWalking;
}

void ACharacterPawn::StartAimDownSight()
{
	bIsAimingDownSight = true;
}

void ACharacterPawn::EndAimDownSight()
{
	bIsAimingDownSight = false;
}

bool ACharacterPawn::IsAimingDownSight() const
{
	return bIsAimingDownSight;
}

void ACharacterPawn::StartSprint()
{
	bIsSprinting = true;
}

void ACharacterPawn::EndSprint()
{
	bIsSprinting = false;
}

bool ACharacterPawn::IsSprinting() const
{
	return bIsSprinting;
}

bool ACharacterPawn::IsAlive() const
{
	return Health > 0;
}

bool ACharacterPawn::IsDead() const
{
	return bIsDying || Health <= 0;
}

float ACharacterPawn::GetHealth() const
{
	return Health;
}

float ACharacterPawn::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (Health <= 0.f)
	{
		return 0.f;
	}
	
	UAISense_Damage::ReportDamageEvent(this, this, DamageCauser, Damage, DamageCauser->GetActorLocation(), GetActorLocation());

	const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	
	if (ActualDamage > 0.f)
	{
		Health -= ActualDamage;

		if (Health <= 0)
		{

			DetachFromControllerPendingDestroy();

			UCapsuleComponent* CapsuleComp = GetCapsuleComponent();
			CapsuleComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			CapsuleComp->SetCollisionResponseToAllChannels(ECR_Ignore);

			USkeletalMeshComponent* Mesh3P = GetMesh();
			if (Mesh3P)
			{
				Mesh3P->SetCollisionProfileName(TEXT("Ragdoll"));
			}
			SetActorEnableCollision(true);

			SetRagdollPhysics();
			/* Apply physics impulse on the bone of the enemy skeleton mesh we hit (ray-trace damage only) */
			if (DamageEvent.IsOfType(FPointDamageEvent::ClassID))
			{
				FPointDamageEvent PointDmg = *((FPointDamageEvent*)(&DamageEvent));
				{
					//if (GEngine)
					//{
					//	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(PointDmg.HitInfo.BoneName));
					//}
					// TODO: Use DamageTypeClass->DamageImpulse
					Mesh3P->AddImpulseAtLocation(PointDmg.ShotDirection * 12000, PointDmg.HitInfo.ImpactPoint, PointDmg.HitInfo.BoneName);
				}
			}
			if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
			{
				FRadialDamageEvent RadialDmg = *((FRadialDamageEvent const*)(&DamageEvent));
				{
					Mesh3P->AddRadialImpulse(RadialDmg.Origin, RadialDmg.Params.GetMaxRadius(), 100000 /*RadialDmg.DamageTypeClass->DamageImpulse*/, ERadialImpulseFalloff::RIF_Linear);
				}
			}
		}
	}
	
	return ActualDamage;
}

bool ACharacterPawn::CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const
{
	return false;
}

bool ACharacterPawn::Die(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser)
{
	return false;
}

void ACharacterPawn::OnDeath(float KillingDamage, FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser)
{
}

void ACharacterPawn::PlayHit(float DamageTaken, FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser, bool bKilled)
{

}

void ACharacterPawn::SetRagdollPhysics()
{
	bool bInRagdoll = false;
	USkeletalMeshComponent* Mesh3P = GetMesh();

	if (IsPendingKill())
	{
		bInRagdoll = false;
	}
	else if (!Mesh3P || !Mesh3P->GetPhysicsAsset())
	{
		bInRagdoll = false;
	}
	else
	{
		Mesh3P->SetAllBodiesSimulatePhysics(true);
		Mesh3P->SetSimulatePhysics(true);
		Mesh3P->WakeAllRigidBodies();
		Mesh3P->bBlendPhysics = true;

		bInRagdoll = true;
	}

	/*UCharacterMovementComponent* CharacterComp = Cast<UCharacterMovementComponent>(GetMovementComponent());*/
	if (CharacterPawnMovementComp)
	{
		CharacterPawnMovementComp->StopMovementImmediately();
		CharacterPawnMovementComp->DisableMovement();
		CharacterPawnMovementComp->SetComponentTickEnabled(false);
	}

	if (!bInRagdoll)
	{
		// Immediately hide the pawn
		TurnOff();
		SetActorHiddenInGame(true);
		SetLifeSpan(1.0f);
	}
	else
	{
		SetLifeSpan(3.0f);
	}
}

void ACharacterPawn::StartFireWeapon()
{
	if (EquippedWeapon) 
	{
		EquippedWeapon->FireWeapon();
		UAISense_Hearing::ReportNoiseEvent(this, GetActorLocation(), EquippedWeapon->FireSoundVolume, GetOwner(), EquippedWeapon->FireSoundRange, NAME_None);
	}
}

void ACharacterPawn::StopFireWeapon()
{

}



//void ACharacterPawn::StartAimAtTarget()
//{
//	bIsAImingDownSight = true;
//}
//
//void ACharacterPawn::StopAimAtTarget()
//{
//	bIsAImingDownSight = false;
//}
