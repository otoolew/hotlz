// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include "HotLZ.h"
#include "GenericTeamAgentInterface.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Components/DecalComponent.h"
#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/GameEngine.h"
#include "Weapon/RangedWeapon.h"
#include "InventoryComponent.h"
#include "Interactable.h"
#include "DrawDebugHelpers.h"

APlayerPawn::APlayerPawn(const class FObjectInitializer& ObjectInitializer) : ACharacterPawn(ObjectInitializer)
{	
	GetCharacterMovement()->bOrientRotationToMovement = false;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->bAbsoluteRotation = true;
	//CameraBoom->TargetArmLength = 1200.0f;
	CameraBoom->bUsePawnControlRotation = false;
	//CameraSpringArm->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComp->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComp->bUsePawnControlRotation = false;
	//PlayerCameraComponent->bAbsoluteLocation = true;
	//PlayerCameraComponent->bUseAttachParentBound = true;

	//// Create a decal in the world to show the cursor's location
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	//CursorToWorld->SetupAttachment(RootComponent);
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());
	//CursorToWorld->SetVisibility(false);

	InventoryComp = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComp"));
}

void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();

	//if (GEngine) {
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Some debug message!" + rot.ToString()));
	//}
}

void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (bIsAimingDownSight) 
	{
		//CursorMovement();
		AimToCursor();
	}
	else 
	{
		PlayerRotation();
	}
	//CursorMovement();

}

void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Movement
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerPawn::MoveRight);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ACharacterPawn::StartCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ACharacterPawn::EndCrouch);

	PlayerInputComponent->BindAction("AimDownSight", IE_Pressed, this, &ACharacterPawn::StartAimDownSight);
	PlayerInputComponent->BindAction("AimDownSight", IE_Released, this, &ACharacterPawn::EndAimDownSight);

	PlayerInputComponent->BindAction("FireWeapon", IE_Pressed, this, &APlayerPawn::StartFireWeapon);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ACharacterPawn::StartSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ACharacterPawn::EndSprint);

	PlayerInputComponent->BindAction("Use", IE_Pressed, this, &APlayerPawn::InteractWithObject);

}
//void APlayerPawn::TogglePauseMenu()
//{
//
//	UGameplayStatics::GetGameMode
//}
//void APlayerPawn::CursorMovement()
//{
//	if (CursorToWorld != nullptr)
//	{
//		APlayerController* PlayerController = Cast<APlayerController>(GetController());
//		if (PlayerController != nullptr)
//		{
//			FVector EyeLocation;
//			FRotator EyeRotation;
//			PlayerController->GetActorEyesViewPoint(EyeLocation, EyeRotation);
//
//			FVector LookDirection = EyeRotation.Vector();
//			FVector TraceEnd = EyeLocation + (LookDirection * 10000);
//
//			FCollisionQueryParams QueryParams;
//			QueryParams.AddIgnoredActor(GetOwner());
//			QueryParams.AddIgnoredActor(this);
//			QueryParams.bTraceComplex = true;
//			QueryParams.bReturnPhysicalMaterial = true;
//
//			FHitResult TraceHitResult;
//			if (GetWorld()->LineTraceSingleByChannel(TraceHitResult, EyeLocation, TraceEnd, ECC_Visibility, QueryParams))
//			{
//				FVector CursorFV = TraceHitResult.ImpactNormal;
//				FRotator CursorR = CursorFV.Rotation();
//				CursorToWorld->SetWorldLocation(TraceHitResult.ImpactPoint);
//				CursorToWorld->SetWorldRotation(CursorR);
//			}
//
//			//PlayerController->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
//			//FVector CursorFV = TraceHitResult.ImpactNormal;
//			//FRotator CursorR = CursorFV.Rotation();
//			//CursorToWorld->SetWorldLocation(TraceHitResult.Location);
//			//CursorToWorld->SetWorldRotation(CursorR);
//		}
//	}
//}

void APlayerPawn::AimToCursor()
{
	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	if (PlayerController != nullptr)
	{
		FHitResult TraceHitResult;

		PlayerController->GetHitResultUnderCursor(COLLISION_WEAPON, true, TraceHitResult);
		FVector EyeLocation;
		FRotator EyeRotation;
		GetActorEyesViewPoint(EyeLocation, EyeRotation);
		FRotator lookRotation = UKismetMathLibrary::FindLookAtRotation(EyeLocation, TraceHitResult.Location);

		FRotator rot = FMath::RInterpTo(GetActorRotation(), lookRotation, GetWorld()->GetDeltaSeconds(), 360.0f);

		PlayerController->SetControlRotation(rot);
	}
}

void APlayerPawn::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(FVector(1, 0, 0), Value);
	}
	//if ((Controller != NULL) && (Value != 0.0f))
	//{
	//	// find out which way is forward
	//	const FRotator Rotation = Controller->GetControlRotation();
	//	const FRotator YawRotation(0, Rotation.Yaw, 0);

	//	// get forward vector
	//	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	//	AddMovementInput(Direction, Value);
	//}
}

void APlayerPawn::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(FVector(0, 1, 0), Value);
	}
}

void APlayerPawn::PlayerRotation()
{
	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	if (PlayerController != nullptr)
	{
		FHitResult TraceHitResult;

		PlayerController->GetHitResultUnderCursor(ECC_WorldStatic, true, TraceHitResult);

		FRotator lookRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), TraceHitResult.Location);

		const FRotator LookRotation(0, lookRotation.Yaw, 0);

		PlayerController->SetControlRotation(LookRotation);
	}
}



void APlayerPawn::InteractWithObject()
{
	FVector EyeLocation;
	FRotator EyeRotation;
	GetActorEyesViewPoint(EyeLocation, EyeRotation);
	FVector TraceDirection = EyeRotation.Vector();
	FVector TraceEnd = EyeLocation + (TraceDirection * 1000);

	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(this);
	QueryParams.bTraceComplex = true;
	//QueryParams.bReturnPhysicalMaterial = true;

	FHitResult Hit(ForceInit);
	if (GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, ECollisionChannel::ECC_Visibility, QueryParams))
	{

		AActor* HitActor = Hit.GetActor();
		IInteractable* TheInterface = Cast<IInteractable>(HitActor);
		if (TheInterface)
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Interface Hit"));
			}
			//Don't call your functions directly, use the 'Execute_' prefix
			//the Execute_ReactToHighNoon and Execute_ReactToMidnight are generated on compile
			//you may need to compile before these functions will appear
			TheInterface->Execute_OnInteraction(HitActor);
		}
	}
	DrawDebugLine(GetWorld(), EyeLocation, TraceEnd, FColor::Red, false, 10.0f, 0, 1.0f);

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("I am interacting!"));
	}
}

void APlayerPawn::StartAimDownSight()
{
	bIsAimingDownSight = true;
	//CursorToWorld->SetVisibility(true);
	//APlayerController* PlayerController = Cast<APlayerController>(GetController());
	//if (PlayerController != nullptr)
	//{
	//	PlayerController->bShowMouseCursor = false;
	//}
}

void APlayerPawn::EndAimDownSight()
{
	bIsAimingDownSight = false;
	//CursorToWorld->SetVisibility(false);
	//APlayerController* PlayerController = Cast<APlayerController>(GetController());
	//if (PlayerController != nullptr)
	//{
	//	PlayerController->bShowMouseCursor = true;
	//}
}
