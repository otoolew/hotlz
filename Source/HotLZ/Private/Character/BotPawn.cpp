// Fill out your copyright notice in the Description page of Project Settings.


#include "BotPawn.h"
#include "Perception/AIPerceptionComponent.h"

ABotPawn::ABotPawn(const FObjectInitializer& ObjectInitializer) : ACharacterPawn(ObjectInitializer)
{	
	bUseControllerRotationYaw = true;	
}

//void ABotPawn::StartAimAtTarget()
//{
//	ACharacterPawn::StartAimAtTarget();
//}
//
//void ABotPawn::StopAimAtTarget()
//{
//	ACharacterPawn::StopAimAtTarget();
//}
