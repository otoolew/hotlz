// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseWeapon.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"


#include "Engine/GameEngine.h"

// Sets default values
ABaseWeapon::ABaseWeapon(const class FObjectInitializer& ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	AIPerceptionStimuliSourceComp = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>("AIPerceptionStimuliSource");
}

// Called when the game starts or when spawned
void ABaseWeapon::BeginPlay()
{
	AActor::BeginPlay();
}

// Called every frame
void ABaseWeapon::Tick(float DeltaTime)
{
	AActor::Tick(DeltaTime);
}

void ABaseWeapon::FireWeapon()
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Fire Weapon!"));
	}

}

