// Fill out your copyright notice in the Description page of Project Settings.


#include "RangedWeapon.h"
#include "HotLZ.h"
#include "Engine/GameEngine.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Perception/AISense_Hearing.h"
//#include "DrawDebugHelpers.h"

// Sets default values
ARangedWeapon::ARangedWeapon(const class FObjectInitializer& ObjectInitializer): ABaseWeapon(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	//AIPerceptionStimuliSourceComp = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>("AIPerceptionStimuliSource");

	WeaponType = EWeaponType::Ranged;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->bCastDynamicShadow = true;
	WeaponMesh->CastShadow = true;
	WeaponMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	MuzzleSocketName = "MuzzleSocket";
	MuzzlePoint = CreateDefaultSubobject<UArrowComponent>(TEXT("MuzzlePoint"));
	MuzzlePoint->SetupAttachment(WeaponMesh);
	MuzzlePoint->AttachToComponent(WeaponMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, MuzzleSocketName);

/*	LaserBeamEffect = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("LaserBeamEffect"));
	LaserBeamEffect->AttachToComponent(MuzzlePoint, FAttachmentTransformRules::SnapToTargetNotIncludingScale);*/	
	//LaserBeamComp->AttachToComponent(MuzzlePoint, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
}

// Called when the game starts or when spawned
void ARangedWeapon::BeginPlay()
{
	AActor::BeginPlay();
	//DeactivateLaserBeam();
}

// Called every frame
void ARangedWeapon::Tick(float DeltaTime)
{
	AActor::Tick(DeltaTime);
}

void ARangedWeapon::PlayMuzzleEffects()
{
	if (MuzzleEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, WeaponMesh, MuzzleSocketName);
	}
}

void ARangedWeapon::PlayBulletTrailEffect(FVector EndPoint)
{
	if (BulletTrailEffect)
	{
		FVector MuzzleLocation = WeaponMesh->GetSocketLocation(MuzzleSocketName);

		FVector AdditionalForward(100.0f, 0.0f, 0.0f);
		FVector StartLocation = UKismetMathLibrary::Add_VectorVector(WeaponMesh->GetSocketLocation(MuzzleSocketName), GetActorForwardVector() * 100);
		UParticleSystemComponent* BulletTrailComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BulletTrailEffect, StartLocation);

		if (BulletTrailComp) 
		{
			BulletTrailComp->SetVectorParameter("Target", EndPoint);
		}
		
	}
}

void ARangedWeapon::PlayImpactEffect(FHitResult Hit)
{
	if (ImpactEffect_Default)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect_Default, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
	}


	//UPhysicalMaterial* HitPhysMat = Hit.PhysMaterial.Get();
	//UParticleSystem* SelectedEffect = nullptr;
	//if (SelectedEffect)
	//{
	//	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SelectedEffect, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
	//}
}

void ARangedWeapon::FireWeapon()
{
	ABaseWeapon::FireWeapon();
	//if (GEngine)
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Fire Weapon!"));
	//}
	UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());

	UAISense_Hearing::ReportNoiseEvent(GetOwner(), GetActorLocation(), FireSoundVolume, GetOwner(), FireSoundRange, NAME_None);

	AActor* ActorOwner = GetOwner();

	if (ActorOwner)
	{
		FVector EyeLocation;
		FRotator EyeRotation;
		ActorOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FVector ShotDirection = EyeRotation.Vector();

		float HalfRad = FMath::DegreesToRadians(BulletSpread);
		ShotDirection = FMath::VRandCone(ShotDirection, HalfRad, HalfRad);

		FVector TraceEnd = EyeLocation + (ShotDirection * 10000);

		FVector BulletTrailEndPoint = TraceEnd;

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(ActorOwner);
		QueryParams.AddIgnoredActor(this);
		QueryParams.bTraceComplex = true;
		QueryParams.bReturnPhysicalMaterial = true;

		FHitResult Hit;
		if (GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, COLLISION_WEAPON, QueryParams))
		{
			AActor* HitActor = Hit.GetActor();
			UGameplayStatics::ApplyPointDamage(HitActor, 20.0f, ShotDirection, Hit, ActorOwner->GetInstigatorController(), this, DamageType);

			PlayImpactEffect(Hit);
			BulletTrailEndPoint = Hit.ImpactPoint;
		}
		//DrawDebugLine(GetWorld(), EyeLocation, TraceEnd, FColor::White, false, 10.0f, 0, 1.0f);

		PlayMuzzleEffects();
		PlayBulletTrailEffect(BulletTrailEndPoint);
	}

}
//bool ARangedWeapon::IsLaserBeamActive() const
//{
//	return LaserBeamEffect->HasBegunPlay();
//}
//
//void ARangedWeapon::ActivateLaserBeam()
//{
//	if (LaserBeamEffect)
//	{
//		LaserBeamEffect->ActivateSystem();
//	}
//}
//
//void ARangedWeapon::DeactivateLaserBeam()
//{
//	if (LaserBeamEffect)
//	{
//		LaserBeamEffect->DeactivateSystem();
//	}
//}
//
//void ARangedWeapon::SetLaserBeamTarget(FVector Location)
//{
//	if (LaserBeamEffect)
//	{
//		LaserBeamEffect->SetBeamTargetPoint(0, Location, 0);
//	}
//}