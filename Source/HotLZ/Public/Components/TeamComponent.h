// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TeamComponent.generated.h"

UENUM(BlueprintType)
enum class ETeamFaction : uint8
{
	Neutral,
	PlayerFaction,
	Anarchist,
	Good,
	Evil
};
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HOTLZ_API UTeamComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTeamComponent(const class FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Team")
	ETeamFaction TeamFaction;

	UFUNCTION(BlueprintCallable, Category = "Team")
	bool IsHostile(ETeamFaction Value) const;
		
};
