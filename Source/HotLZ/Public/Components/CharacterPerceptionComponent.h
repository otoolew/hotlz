// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Perception/AIPerceptionComponent.h"
#include "CharacterPerceptionComponent.generated.h"

class UAISenseConfig_Sight;
class UAISense_Damage;

/**
 * 
 */
UCLASS(Blueprintable, meta = (BlueprintSpawnableComponent))
class HOTLZ_API UCharacterPerceptionComponent : public UAIPerceptionComponent
{
	GENERATED_BODY()
public:
	UCharacterPerceptionComponent(const class FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI Perception", meta = (AllowPrivateAccess = "true"))
	UAISenseConfig_Sight* SightSense;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI Perception", meta = (AllowPrivateAccess = "true"))
	//TSubclassOf<UAISense_Sight*> SightSense;
	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI Perception", meta = (AllowPrivateAccess = "true"))
	//UAISense_Damage* DamageSense;
};
