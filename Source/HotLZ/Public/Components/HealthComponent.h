// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams(FOnHealthChangedSignature, UHealthComponent*, HealthComp, float, Health, float, HealthChangeValue, const class UDamageType*, DamageType, class AController*, InstegatedBy, AActor*, DamageCauser);

UCLASS(meta=(BlueprintSpawnableComponent))
class HOTLZ_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent(const class FObjectInitializer& ObjectInitializer);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HealthComponent")
	float MaxHealth;

	UPROPERTY(BlueprintReadOnly, Category = "HealthComponent")
	float Health;


public:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstegatedBy, AActor* DamageCauser);

	UFUNCTION(BlueprintCallable, Category = "HealthComponent")
	bool IsAlive() const;

	UFUNCTION(BlueprintCallable, Category = "HealthComponent")
	bool IsDying() const;

	UFUNCTION(BlueprintCallable, Category = "HealthComponent")
	bool IsDead() const;

	UFUNCTION(BlueprintPure, Category = "HealthComponent")
	float GetHealth() const;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnHealthChangedSignature OnHealthChanged;

};
