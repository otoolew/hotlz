// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CharacterPawnMovementComponent.generated.h"

//enum EPostUpdateMode
//{
//	PostUpdate_Record,		// Record a move after having run the simulation
//	PostUpdate_Replay,		// Update after replaying a move for a client correction
//};

/**
 * 
 */
UCLASS()
class HOTLZ_API UCharacterPawnMovementComponent : public UCharacterMovementComponent
{
	GENERATED_UCLASS_BODY()
public:

	friend class FSavedMove_CharacterPawnMovement;

	virtual void UpdateFromCompressedFlags(uint8 Flags) override;

	virtual class FNetworkPredictionData_Client* GetPredictionData_Client() const override;


//============================================================================================
//Replication
//============================================================================================
	class FSavedMove_CharacterPawnMovement : public FSavedMove_Character
	{
	public:

		typedef FSavedMove_Character Super;

		///@brief Resets all saved variables.
		virtual void Clear() override;

		///@brief Store input commands in the compressed flags.
		virtual uint8 GetCompressedFlags() const override;

		///@brief This is used to check whether or not two moves can be combined into one.
		///Basically you just check to make sure that the saved variables are the same.
		virtual bool CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const override;

		///@brief Sets up the move before sending it to the server. 
		virtual void SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character& ClientData) override;
		///@brief Sets variables on character movement component before making a predictive correction.
		virtual void PrepMoveFor(class ACharacter* Character) override;
		
		FVector SavedMoveDirection;

		uint8 bSavedWantsToCasuallyWalk : 1;	
		uint8 bSavedWantsToAimDownSight : 1;
		uint8 bSavedWantsToSprint : 1;

	};

	class FNetworkPredictionData_Client_CharacterPawnMovement : public FNetworkPredictionData_Client_Character
	{
	public:
		FNetworkPredictionData_Client_CharacterPawnMovement(const UCharacterMovementComponent& ClientMovement);

		typedef FNetworkPredictionData_Client_Character Super;

		///@brief Allocates a new copy of our custom saved move
		virtual FSavedMovePtr AllocateNewMove() override;
	};

public:

	FVector MoveDirection;
	///@brief Override maximum speed during sprint.
	virtual float GetMaxSpeed() const override;
	///@brief Override maximum speed during sprint.
	virtual float GetMoveVelocity() const; 
	virtual float GetDirection() const;
	///@brief Override maximum acceleration for sprint.
	virtual float GetMaxAcceleration() const override;



	virtual bool HandlePendingLaunch() override;


	void OnMovementUpdated(float DeltaTime, const FVector& OldLocation, const FVector& OldVelocity);

//============================================================================================
// Casual Walk
//============================================================================================
	UPROPERTY(EditAnywhere, Category = "Casual")
	float CasualWalkSpeedMultiplier;

	UPROPERTY(EditAnywhere, Category = "Casual")
	float CasualWalkAccelerationMultiplier;

	///@brief Activate or deactivate sprint.
	void SetCasualWalk(bool bCasuallyWalking);

	///@brief Flag for activating sprint.
	uint8 bWantsToCasuallyWalk : 1;

//============================================================================================
// Aim Down Sights
//============================================================================================

	UPROPERTY(EditDefaultsOnly, Category = "Aim Down Sights")
	float AimDownSightSpeedMultiplier;

	UPROPERTY(EditAnywhere, Category = "Aim Down Sights")
	float AimDownSightAccelerationMultiplier;

	///@brief Activate or deactivate sprint.
	void SetAimDownSight(bool bAimingDownSight);

	///@brief Flag for activating ADS.
	uint8 bWantsToAimDownSight : 1;

//============================================================================================
// Sprint
//============================================================================================
	UPROPERTY(EditAnywhere, Category = "Sprint")
	float SprintSpeedMultiplier;

	UPROPERTY(EditAnywhere, Category = "Sprint")
	float SprintAccelerationMultiplier;

	///@brief Activate or deactivate sprint.
	void SetSprinting(bool bSprinting);

	///@brief Flag for activating sprint.
	uint8 bWantsToSprint : 1;

	UFUNCTION(BlueprintCallable, Category = "Sprint")
	bool IsMovingForward() const;
};
