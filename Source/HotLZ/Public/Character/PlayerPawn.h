// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/CharacterPawn.h"
#include "GenericTeamAgentInterface.h"
#include "PlayerPawn.generated.h"

struct FGenericTeamId;
class UCameraComponent;
class USpringArmComponent;
//class UDecalComponent;
class UInventoryComponent;
/**
 * 
 */
UCLASS()
class HOTLZ_API APlayerPawn : public ACharacterPawn
{
	GENERATED_BODY()
public:
	APlayerPawn(const class FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

/************************************************************************/
/* UI and Menu                                                          */
/************************************************************************/
	//void TogglePauseMenu();


/************************************************************************/
/* Mouse Cursor                                                         */
/************************************************************************/
//private:
//	/** A decal that projects to the cursor location. */
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Cursor", meta = (AllowPrivateAccess = "true"))
//	class UDecalComponent* CursorToWorld;
//
//public:
//	/** Returns CursorToWorld subobject **/
//	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	//void CursorMovement();
	void AimToCursor();

/************************************************************************/
/* Camera                                                               */
/************************************************************************/
private:
	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

public:
	///** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetCameraComponent() const { return CameraComp; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
/************************************************************************/
/* Movement                                                             */
/************************************************************************/
public:
	virtual void MoveForward(float value);

	virtual void MoveRight(float value);

	void PlayerRotation();


/************************************************************************/
/* Interact                                                             */
/************************************************************************/
	void InteractWithObject();
	//UFUNCTION(BlueprintCallable, Category = "Collision", meta = (UnsafeDuringActorConstruction = "true"))
	//	void GetOverlappingActors(TArray<AActor*>& OverlappingActors, TSubclassOf<AActor> ClassFilter = nullptr) const;
///************************************************************************/
///* Weapon                                                               */
///************************************************************************/
public:
	void StartAimDownSight() override;

	void EndAimDownSight() override;

///************************************************************************/
///* Inventory                                                               */
///************************************************************************/
protected:

	UPROPERTY(EditAnywhere, Category = "Inventory")
	UInventoryComponent* InventoryComp;
public:
	FORCEINLINE class UInventoryComponent* GetInventoryComponent() const { return InventoryComp; }
};
