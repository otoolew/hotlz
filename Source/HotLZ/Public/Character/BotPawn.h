// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/CharacterPawn.h"
#include "BotPawn.generated.h"

//class UAIPerceptionComponent;
/**
 * 
 */
UCLASS()
class HOTLZ_API ABotPawn : public ACharacterPawn
{
	GENERATED_BODY()
public:
	ABotPawn(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* BotBehavior;

	UPROPERTY(BlueprintReadWrite, Category = "AI")
    bool bInCoverPosition;

	//virtual void StartAimAtTarget()override;

	//virtual void StopAimAtTarget()override;
};
