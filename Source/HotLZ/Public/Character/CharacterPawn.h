// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HotLZ.h"
#include "GameFramework/Character.h"
#include "CharacterPawn.generated.h"

class UAIPerceptionStimuliSourceComponent;
class ABaseWeapon;
class UHealthComponent;
class UCharacterPawnMovementComponent;


UCLASS(ABSTRACT)
class HOTLZ_API ACharacterPawn : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterPawn(const class FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual FVector GetPawnViewLocation() const override;

	virtual void PostInitializeComponents() override;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character Stats")
	struct FCharacterData CharacterData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character State")
	ECharacterState CharacterState;


protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Character State")
	bool bIsInCombat;
private:
	UFUNCTION(BlueprintCallable, Category = "PlayerCondition")
	bool IsInCombat() const;

//public:
//	UFUNCTION()
//	void TouchSensed(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
/************************************************************************/
/* Mobility                                                             */
/************************************************************************/
private:
	UCharacterPawnMovementComponent* CharacterPawnMovementComp;

protected:

	float MoveSpeed;

	float MoveDirection;

public:
	
	UFUNCTION(BlueprintCallable, Category = "Movement")
		FORCEINLINE class UCharacterPawnMovementComponent* GetCharacterPawnMovementComponent() const { return CharacterPawnMovementComp; }

	UFUNCTION(BlueprintCallable, Category = "Movement")
	virtual void StartCrouch();
	
	UFUNCTION(BlueprintCallable, Category = "Movement")
	virtual void EndCrouch();

// Walking
	UPROPERTY(BlueprintReadOnly, Category = "Movement")
	bool bIsCasuallyWalking;

	UFUNCTION(BlueprintCallable, Category = "Movement")
	virtual void StartCasualWalk();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	virtual void EndCasualWalk();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	bool IsCasuallyWalking() const;

// Aiming Down Sight
	UPROPERTY(BlueprintReadOnly, Category = "Movement")
	bool bIsAimingDownSight;

	UFUNCTION(BlueprintCallable, Category = "Movement")
	virtual void StartAimDownSight();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	virtual void EndAimDownSight();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	bool IsAimingDownSight() const;

// Sprinting
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	bool bIsSprinting;

	UFUNCTION(BlueprintCallable, Category = "Movement")
	virtual void StartSprint();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	virtual void EndSprint();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	bool IsSprinting() const;

/************************************************************************/
/* Health                                                               */
/************************************************************************/
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PlayerCondition")
	float MaxHealth;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PlayerCondition")
	float Health;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerCondition")
	bool bIsDying;

	UFUNCTION(BlueprintCallable, Category = "PlayerCondition")
	bool IsAlive() const;

	UFUNCTION(BlueprintCallable, Category = "PlayerCondition")
	bool IsDead() const;

	UFUNCTION(BlueprintPure, Category = "PlayerCondition")
	float GetHealth() const;



	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	virtual bool CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const;

	virtual bool Die(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser);

	virtual void OnDeath(float KillingDamage, FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser);

	virtual void PlayHit(float DamageTaken, struct FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser, bool bKilled);

	void SetRagdollPhysics();

/************************************************************************/
/* Weapon                                                               */
/************************************************************************/

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<ABaseWeapon> StartingWeapon;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName WeaponAttachSocketName;

	ABaseWeapon* EquippedWeapon;

	FORCEINLINE class ABaseWeapon* GetEquippedWeapon() const { return EquippedWeapon; }

public:
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void StartFireWeapon();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void StopFireWeapon();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI Perception")
	UAIPerceptionStimuliSourceComponent* AIPerceptionStimuliSourceComp;

public:
	/** Returns HealthComponent subobject **/
	FORCEINLINE class UAIPerceptionStimuliSourceComponent* GetAIPerceptionStimuliSourceComp() { return AIPerceptionStimuliSourceComp; }
};
