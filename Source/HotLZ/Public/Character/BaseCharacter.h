// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

class UAIPerceptionStimuliSourceComponent;
class ABaseWeapon;
class UHealthComponent;
class UCameraComponent;
class USpringArmComponent;
//class UDecalComponent;

UCLASS()
class HOTLZ_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()
public:
	// Sets default values for this character's properties
	ABaseCharacter(const class FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual FVector GetPawnViewLocation() const override;

/************************************************************************/
/* AI                                                                   */
/************************************************************************/
private:
	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player", meta = (AllowPrivateAccess = "true"))
	bool bPossedByPlayer;

public:
	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* BotBehavior;

/************************************************************************/
/* Camera                                                               */
/************************************************************************/
private:
	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

public:
	///** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetCameraComponent() const { return CameraComp; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

/************************************************************************/
/* Aim Cursor                                                           */
/************************************************************************/
//private:
//	/** A decal that projects to the cursor location. */
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Cursor", meta = (AllowPrivateAccess = "true"))
//	class UDecalComponent* CursorToWorld;
//
//public:
//	/** Returns CursorToWorld subobject **/
//	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

//	void PlayerCursorInput();

/************************************************************************/
/* Mobility                                                             */
/************************************************************************/

protected:

	float MoveSpeed;

	float MoveDirection;

	/** modifier for max movement speed */
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	float AimDownSightSpeedModifier;

	/** modifier for max movement speed */
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	float SprintingSpeedModifier;

public:

	UFUNCTION(BlueprintCallable, Category = "Movement")
	float GetSpeed() const;

	UFUNCTION(BlueprintCallable, Category = "Movement")
	float GetDirection() const;

	virtual void MoveForward(float value);

	virtual void MoveRight(float value);

	void StartCrouch();

	void EndCrouch();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	float GetAimDownSightSpeedModifier() const;

	UFUNCTION(BlueprintCallable, Category = "Movement")
	float GetSprintingSpeedModifier() const;

/************************************************************************/
/* Health                                                               */
/************************************************************************/
protected:
	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerCondition", meta = (AllowPrivateAccess = "true"))
	UHealthComponent* HealthComp;

	UFUNCTION()
	void OnHealthChanged(UHealthComponent* HealthComponent, float CurrentHealth, float HealthChangeValue, const class UDamageType* DamageType, class AController* InstegatedBy, AActor* DamageCauser);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerCondition")
	bool bIsDead;

	/** switch to ragdoll */
	void SetRagdollPhysics();

public:
	/** Returns HealthComponent subobject **/
	FORCEINLINE class UHealthComponent* GetHealthComponent() { return HealthComp; }

/************************************************************************/
/* Weapon                                                               */
/************************************************************************/
protected:

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<ABaseWeapon> StartingWeapon;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName WeaponAttachSocketName;

	ABaseWeapon* EquippedWeapon;

	FORCEINLINE class ABaseWeapon* GetEquippedWeapon() const { return EquippedWeapon; }

public:

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void StartFireWeapon();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void StopFireWeapon();

/************************************************************************/
/* Aim Down Sights                                                      */
/************************************************************************/
protected:

	UPROPERTY(BlueprintReadOnly, Category = "Weapon/AimDownSight")
	bool bIsAImingDownSight;

public:

	UFUNCTION(BlueprintCallable, Category = "Weapon/AimDownSight")
	bool IsAimingDownSight() const;

	UFUNCTION(BlueprintCallable, Category = "Weapon/AimDownSight")
	virtual void StartAimAtTarget();

	UFUNCTION(BlueprintCallable, Category = "Weapon/AimDownSight")
	virtual void StopAimAtTarget();

	UFUNCTION(BlueprintCallable, Category = "Weapon/AimDownSight")
	virtual void FocusAimAtTarget();

/************************************************************************/
/* Perception                                                           */
/************************************************************************/
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI Perception")
	UAIPerceptionStimuliSourceComponent* AIPerceptionStimuliSourceComp;

public:
	/** Returns HealthComponent subobject **/
	FORCEINLINE class UAIPerceptionStimuliSourceComponent* GetAIPerceptionStimuliSourceComp() { return AIPerceptionStimuliSourceComp; }
};
