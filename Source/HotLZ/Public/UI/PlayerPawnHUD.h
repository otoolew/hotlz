// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "PlayerPawnHUD.generated.h"

/**
 * 
 */
UCLASS()
class HOTLZ_API APlayerPawnHUD : public AHUD
{
	GENERATED_BODY()

public:
	APlayerPawnHUD(const class FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	virtual void DrawHUD() override;

private:

	UPROPERTY(EditAnywhere, Category = "Health")
	TSubclassOf<class UUserWidget> HUDWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PlayerCondition", meta = (AllowPrivateAccess = "true"))
	class UUserWidget* CurrentHealthWidget;

//
//	/** Main HUD update loop. */
//	virtual void DrawHUD() override;
//
//	/**
//	* Sent from pawn hit, used to calculate hit notification overlay for drawing.
//	*
//	* @param	DamageTaken		The amount of damage.
//	* @param	DamageEvent		The actual damage event.
//	* @param	PawnInstigator	The pawn that did the damage.
//	*/
//	void NotifyWeaponHit(float DamageTaken, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator);
//
//	/** Notifies we have hit the enemy. */
//	void NotifyEnemyHit();
//
//protected:
//	/** Scoreboard widget. */
//	//TSharedPtr<class SShooterScoreboardWidget>	ScoreboardWidget;
//		/** Draws weapon crosshair. */
//	void DrawCrosshair();
};
