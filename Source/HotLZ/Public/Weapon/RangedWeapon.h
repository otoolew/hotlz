// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "RangedWeapon.generated.h"

class USkeletalMeshComponent;
class UDamageType;
class UParticleSystem;
class UParticleSystemComponent;
class UArrowComponent;
class UAIPerceptionStimuliSourceComponent;

/**
 * 
 */
UCLASS()
class HOTLZ_API ARangedWeapon : public ABaseWeapon
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARangedWeapon(const class FObjectInitializer& ObjectInitializer);

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float ShotsPerMinute;

	/* Bullet Spread in degrees Affected by Character Accuracy Stat*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin=0.0f))
	float BulletSpread;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UArrowComponent* MuzzlePoint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	FName MuzzleSocketName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* MuzzleEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* ImpactEffect_Default;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* BulletTrailEffect;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon/Attachment")
	//UParticleSystemComponent* LaserBeamEffect;

	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon/Attachment")
	//bool bIsLaserBeamActivated;
//
//protected:
//
//	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI Perception")
//	UAIPerceptionStimuliSourceComponent* AIPerceptionStimuliSourceComp;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void FireWeapon();

	//FORCEINLINE class UAIPerceptionStimuliSourceComponent* GetAIPerceptionStimuliSourceComp() { return AIPerceptionStimuliSourceComp; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void PlayMuzzleEffects();

	virtual void PlayBulletTrailEffect(FVector EndPoint);

	virtual void PlayImpactEffect(FHitResult Hit);

//public:

	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon/Attachment")
	//bool bIsBeamActivated;

	//UFUNCTION(BlueprintCallable, Category = "Weapon/Attachment")
	//bool IsLaserBeamActive() const;

	//UFUNCTION(BlueprintCallable, Category = "Weapon/Attachment")
	//void ActivateLaserBeam();

	//UFUNCTION(BlueprintCallable, Category = "Weapon/Attachment")
	//void DeactivateLaserBeam();

	//UFUNCTION(BlueprintCallable, Category = "Weapon/Attachment")
	//void SetLaserBeamTarget(FVector Location);

};
