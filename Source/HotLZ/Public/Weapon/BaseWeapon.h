// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseWeapon.generated.h"

class UAIPerceptionStimuliSourceComponent;


UENUM()
enum class EWeaponType : uint8
{
	Melee,
	Ranged
};

UENUM()
enum class EWeaponState : uint8
{
	Idle,
	Reloading,
	Firing
};

UCLASS(ABSTRACT, Blueprintable)
class HOTLZ_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ABaseWeapon(const class FObjectInitializer& ObjectInitializer);

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	EWeaponType WeaponType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	EWeaponState WeaponState;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	class USoundBase* FireSound;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = 0.0f))
	float FireSoundVolume;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = 0.0f))
	float FireSoundRange;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PerceptionStimuliSource")
	UAIPerceptionStimuliSourceComponent* AIPerceptionStimuliSourceComp;

public:
	/** Returns HealthComponent subobject **/
	FORCEINLINE class UAIPerceptionStimuliSourceComponent* GetAIPerceptionStimuliSourceComp() { return AIPerceptionStimuliSourceComp; }
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void FireWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
