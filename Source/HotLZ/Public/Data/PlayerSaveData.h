// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "PlayerSaveData.generated.h"

/**
 * 
 */
UCLASS()
class HOTLZ_API UPlayerSaveData : public USaveGame
{
	GENERATED_BODY()
	
};
