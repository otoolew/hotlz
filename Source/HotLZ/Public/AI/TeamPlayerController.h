// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerController.h"
#include "GenericTeamAgentInterface.h"
#include "TeamPlayerController.generated.h"

class APlayerPawn;
class APlayerPawnHUD;
/**
 * 
 */
UCLASS()
class HOTLZ_API ATeamPlayerController : public APlayerController, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:

	ATeamPlayerController(const class FObjectInitializer& ObjectInitializer);

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	/** sets up input */
	virtual void SetupInputComponent() override;

protected:

	virtual void OnPossess(class APawn* InPawn) override;
	virtual void OnUnPossess() override;

public:

	/** Returns a pointer to the players pawn */
	APlayerPawn* PlayerPawn;

	/** get currently possessed pawn */
	UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
	APlayerPawn* GetPlayerPawn() const;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
	FGenericTeamId TeamId;

	/** Assigns Team Agent to given TeamID */
	virtual void SetGenericTeamId(const FGenericTeamId& Value) override;

	/** Retrieve team identifier in form of FGenericTeamId */
	virtual FGenericTeamId GetGenericTeamId() const override;
	/** Retrieved owner attitude toward given Other object */
	virtual ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;

/******************************************/
/* UI and Menu                            */
/******************************************/
//protected:
//	/** shooter in-game menu */
//	TSharedPtr<class FShooterIngameMenu> ShooterIngameMenu;
public:
	/** Returns a pointer to the shooter game hud. May return NULL. */
	APlayerPawnHUD* GetPlayerPawnHUD() const;
	/** Is pause menu currently active? */
	bool IsPauseMenuVisible() const;

	/** Toggle PauseMenu handler */
	void OnTogglePauseMenu();

	/** check if gameplay related actions (movement, weapon usage, etc) are allowed right now */
	bool IsGameInputAllowed() const;

	/** is game menu currently active? */
	bool IsGameMenuVisible() const;

	virtual bool SetPause(bool bPause, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;
};
