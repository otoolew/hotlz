// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryGenerator.h"
#include "GenerateCoverPoints.generated.h"

/**
 * 
 */
UCLASS()
class HOTLZ_API UGenerateCoverPoints : public UEnvQueryGenerator
{
	GENERATED_BODY()

	UGenerateCoverPoints(const class FObjectInitializer& ObjectInitializer);

	virtual void GenerateItems(FEnvQueryInstance& QueryInstance) const override;
};
