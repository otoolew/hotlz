// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TeamAIController.generated.h"

class UBlackboardComponent;
class UBehaviorTreeComponent;
//class UCharacterPerceptionComponent;
//class UAISense_Sight;
//class UAISense_Damage;
/**
 * 
 */
UCLASS(ABSTRACT)
class HOTLZ_API ATeamAIController : public AAIController
{
	GENERATED_BODY()

	ATeamAIController(const class FObjectInitializer& ObjectInitializer);

private:
	UPROPERTY(transient)
	UBlackboardComponent* BlackboardComp;

	/* Cached BT component */
	UPROPERTY(transient)
	UBehaviorTreeComponent* BehaviorComp;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI Perception", meta = (AllowPrivateAccess = "true"))
	//UCharacterPerceptionComponent* CharacterPerceptionComp;

public:

	//UFUNCTION(BlueprintCallable, Category = "AI Perception")
	//	FORCEINLINE class UCharacterPerceptionComponent* GetCharacterPerceptionComponent() const { return CharacterPerceptionComp; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
	FGenericTeamId TeamId;

	/** Assigns Team Agent to given TeamID */
	virtual void SetGenericTeamId(const FGenericTeamId& Value) override;

	/** Retrieve team identifier in form of FGenericTeamId */
	virtual FGenericTeamId GetGenericTeamId() const override;

	/** Retrieved owner attitude toward given Other object */
	virtual ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;


protected:

	virtual void OnPossess(class APawn* InPawn) override;
	virtual void OnUnPossess() override;

public:
	//void Respawn();

	//void CheckAmmo(const class AShooterWeapon* CurrentWeapon);

	UFUNCTION(BlueprintCallable, Category = Behavior)
	void SetEnemy(class ACharacterPawn* InCharacterPawn);

	UFUNCTION(BlueprintCallable, Category = Behavior)
	class ACharacterPawn* GetEnemy() const;

	UFUNCTION(BlueprintCallable, Category = Behavior)
	void SetLineOfSightKey(bool Value);

	/* If there is line of sight to current enemy, start firing at them */
	UFUNCTION(BlueprintCallable, Category = Behavior)
	void AttackEnemy();

	/* If there is line of sight to current enemy, start firing at them */
	UFUNCTION()
	void HandlePawnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);
	// Begin AAIController interface
	/** Update direction AI is looking based on FocalPoint */
	//virtual void UpdateControlRotation(float DeltaTime, bool bUpdatePawn = true) override;
	// End AAIController interface

protected:

	int32 EnemyKeyID;
	int32 HasLineOfSightKeyID;
	/** Handle for efficient management of Respawn timer */
	FTimerHandle TimerHandle_Respawn;

public:
	/** Returns BlackboardComp subobject **/
	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return BlackboardComp; }
	/** Returns BehaviorComp subobject **/
	FORCEINLINE UBehaviorTreeComponent* GetBehaviorComp() const { return BehaviorComp; }
};


