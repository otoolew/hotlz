// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "Engine/EngineTypes.h"
#include "Engine/Texture2D.h"
#include "HotLZ.generated.h"

#define COLLISION_WEAPON	ECC_GameTraceChannel1


UENUM(BlueprintType)
enum class ECharacterState : uint8
{
	PlayerControlled,
	Idle,
	Patrol,
	Alert,
	Combat
};

USTRUCT(BlueprintType)
struct FCharacterData
{
	GENERATED_BODY()

/* Character Name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Stats")
	FString CharacterName;

	FString GetCharacterName()
	{
		return CharacterName;
	}

	void SetCharacterName(const FString Value)
	{
		CharacterName = Value;
	}

/* Strength */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Stats")
	int Strength;

	int GetStrength()
	{
		return Strength;
	}

	void SetStrength(const int Value)
	{
		Strength = Value;
	}

public:
	FCharacterData()
	{
		Strength = 0;
	}
};


USTRUCT(BlueprintType)
struct FItemData
{
	GENERATED_BODY()

	/* Item Name */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Stats")
	FText Name;

	/* Item Description */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Stats")
	FText Description;

	/* Item Texture */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Stats")
	UTexture2D* TextureIcon;

	/* Is Item Stackable */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Stats")
	bool bIsStackable;

	/* Is Item Consumable */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Stats")
	bool bIsConsumable;

	/* Item MaxStackSize */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Stats")
	int MaxStackSize;

public:
	FItemData()
	{
		Name = FText();
		Description = FText();
		TextureIcon = NULL;
		bIsStackable = false;
		bIsConsumable = false;
		MaxStackSize = 1;
	}
};

USTRUCT(BlueprintType)
struct FSlotData
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Data")
	struct FItemData StoredItemData;

	/* Item MaxStackSize */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Stats")
	int Quantity;

public:
	FSlotData()
	{
		StoredItemData = FItemData();
		Quantity = 0;
	}
};


USTRUCT()
struct FTakeHitInfo
{
	GENERATED_BODY()

	UPROPERTY()
	float ActualDamage;

	UPROPERTY()
	UClass* DamageTypeClass;

	UPROPERTY()
	TWeakObjectPtr<class ACharacterPawn> PawnInstigator;

	UPROPERTY()
	TWeakObjectPtr<class AActor> DamageCauser;

	UPROPERTY()
	uint8 DamageEventClassID;

	UPROPERTY()
	bool bKilled;

private:

	UPROPERTY()
	uint8 EnsureReplicationByte;

	UPROPERTY()
	FDamageEvent GeneralDamageEvent;

	UPROPERTY()
	FPointDamageEvent PointDamageEvent;

	UPROPERTY()
	FRadialDamageEvent RadialDamageEvent;

public:
	FTakeHitInfo()
		: ActualDamage(0),
		DamageTypeClass(nullptr),
		PawnInstigator(nullptr),
		DamageCauser(nullptr),
		DamageEventClassID(0),
		bKilled(false),
		EnsureReplicationByte(0)
	{
	}

	FDamageEvent& GetDamageEvent()
	{
		switch (DamageEventClassID)
		{
		case FPointDamageEvent::ClassID:
			if (PointDamageEvent.DamageTypeClass == nullptr)
			{
				PointDamageEvent.DamageTypeClass = DamageTypeClass ? DamageTypeClass : UDamageType::StaticClass();
			}
			return PointDamageEvent;

		case FRadialDamageEvent::ClassID:
			if (RadialDamageEvent.DamageTypeClass == nullptr)
			{
				RadialDamageEvent.DamageTypeClass = DamageTypeClass ? DamageTypeClass : UDamageType::StaticClass();
			}
			return RadialDamageEvent;

		default:
			if (GeneralDamageEvent.DamageTypeClass == nullptr)
			{
				GeneralDamageEvent.DamageTypeClass = DamageTypeClass ? DamageTypeClass : UDamageType::StaticClass();
			}
			return GeneralDamageEvent;
		}
	}


	void SetDamageEvent(const FDamageEvent& DamageEvent)
	{
		DamageEventClassID = DamageEvent.GetTypeID();
		switch (DamageEventClassID)
		{
		case FPointDamageEvent::ClassID:
			PointDamageEvent = *((FPointDamageEvent const*)(&DamageEvent));
			break;
		case FRadialDamageEvent::ClassID:
			RadialDamageEvent = *((FRadialDamageEvent const*)(&DamageEvent));
			break;
		default:
			GeneralDamageEvent = DamageEvent;
		}
	}


	void EnsureReplication()
	{
		EnsureReplicationByte++;
	}
};